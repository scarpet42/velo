#  Cet été, j'ai fait du vélo

Ce projet est destiné à produire un document LaTeX.

## Le document produit raconte mon aller retour Angers Lund à vélo

C'est le premier voyage que j'ai raconté. 
J'avais déjà fait du vélo sur plusieurs jours, mais c'était la première fois que je quittais la France à vélo.
Je suis parti d'Angers (en France près de Nantes), allé à Lund (en Suède près de Malmö) et retourné à Angers.
Ce voyage s'est passé en été 1997, d'où le titre.
Cela a représenté 3200 km en 15 jours de vélo et 11 jours de repos.
À l'époque, je dormais à la belle étoile : je n'utilisais plus ma tente, mais je ne dormais pas encore à l'hôtel.
C'est la première fois que j'écrivais quelque chose, je ne sais pas pourquoi je l'ai fait.
C'est la rédaction de ce récit qui m'a fait découvrir les plaisirs de l'écriture.

## Lecture du document compilé
Le document compilé est disponible ici : [velo.pdf](https://scarpet42.gitlab.io/velo/velo.pdf)

## License

La licence est la WTFPL : [WTFPL](http://www.wtfpl.net/)
C'est la plus permissive des licences.
Si je ne le fais pas, vous n'avez rien le droit de faire du texte sans mon accord.
